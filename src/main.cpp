/*
 * Sample program for DokodeModem
 * ex UART sample
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();
BME280 exSensor = BME280();

void setup()
{
  Dm.begin();            // 初期化が必要です。
  
  Dm.exPowerCtrl(ON);    // 外部電源出力をONにします
  Dm.exComPowerCtrl(ON); // 外部シリアル電源出力をONにします

  UartExternal.begin(115200);//外部UARTを初期化します
  SerialDebug.begin(115200);//USB-UARTを初期化します
}

void loop()
{
  // USB-UARTからのデータを外部UART側に送信します
  while (SerialDebug.available())
  {
    uint8_t data = SerialDebug.read();
    // SerialDebug.write(data); //echo back
    UartExternal.write(data);
  }

  // 外部UARTからのデータをUSBデバッグ側に送信します
  while (UartExternal.available())
  {
    SerialDebug.write(UartExternal.read());
  }
}
